#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <drawtk.h>
#include <dtk_colors.h>
#include <dtk_event.h>
#include <dtk_time.h>

#include <cnbicore/CcTime.hpp>
#include <cnbismr/stimula.h>
#include <cnbicore/CcBasic.hpp>
#include <cnbiloop/ClLoop.hpp>
#include <cnbiloop/ClTobiId.hpp>



/* DRAWS */
#define CIR_RADIUS 	0.1f
#define CIR_NPOINTS	500
#define CIR_COLOR	dtk_white
#define CRO_SIZE 	0.1f
#define CRO_COLOR	dtk_white
#define LIN_SIZE	0.2f
#define LIN_WIDTH	0.05f
#define LIN_COLOR	dtk_white
#define LFT_POSX	-0.8f
#define LFT_POSY	0.0f
#define RGT_POSX	0.8f
#define RGT_POSY	0.0f
#define TOP_POSX	0.0f
#define TOP_POSY	0.8f
#define DWN_POSX	0.0f
#define DWN_POSY	-0.8f

/* Trial parameters */
#define TRL_NUMCLASSES	6
#define TRL_OCCURENCES	10
#define TRL_TIME_START	1000.0f
#define TRL_TIME_PERIOD	2000.0f
#define TRL_TIME_END	1000.0f

/* TRIGGER EVENTS */
#define EVT_TRL_START 		0x0001		// Decimal: 1
#define EVT_TRL_END 		0x0020		// Decimal: 32
#define EVT_TRL_LEFT		0x0010 		// Decimal: 16
#define EVT_TRL_RIGHT		0x0011		// Decimal: 17
#define EVT_TRL_TOP		0x0012		// Decimal: 18
#define EVT_TRL_DOWN		0x0013		// Decimal: 19
#define EVT_TRL_BLINK		0x0014		// Decimal: 20
#define EVT_TRL_FIX		0x0015		// Decimal: 21
#define EVT_TRL_OFF		0x8000		// 


static dtk_hwnd wnd 	= NULL;
static dtk_hshape comp 	= NULL;

dtk_hshape circle 	= NULL;
dtk_hshape cross 	= NULL;
dtk_hshape line 	= NULL;
dtk_hshape text 	= NULL;

dtk_hfont font;

unsigned int WinWidth	= 640;
unsigned int WinHeight 	= 480;
bool waituser 		= true;
const char waittext[] 	= "Ready? Press SPACE to start";
unsigned int event;

ClTobiId* id;
IDMessage* idm;
IDSerializerRapid* ids;

static void hide_shape(dtk_hshape shp) {
	float tmp[4] = {0.0f, 0.0f, 0.0f, 0.0f};
	if (shp != NULL)
		dtk_setcolor_shape(shp, tmp, DTK_IGNRGB);
}

static void show_shape(dtk_hshape shp) {
	float tmp[4] = {0.0f, 0.0f, 0.0f, 1.0f};
	if (shp!=NULL)
		dtk_setcolor_shape(shp, tmp, DTK_IGNRGB);
}

static void setup_shapes(void) {
	font  	= dtk_load_font("arial:style=bold italic");
	
	circle  = dtk_create_circle(NULL, 0.0f, 0.0f, CIR_RADIUS, 1, CIR_COLOR, CIR_NPOINTS);
	cross 	= dtk_create_cross(NULL, 0.0f, 0.0f, CRO_SIZE, CRO_COLOR);
	line 	= dtk_create_rectangle_2p(NULL, -LIN_SIZE/2, -LIN_WIDTH/2, LIN_SIZE/2, LIN_WIDTH/2, 1, LIN_COLOR); 
	text    = dtk_create_string(NULL, waittext, 0.1f, 0.0f, 0.0f, DTK_HMID, dtk_white, font);
	
	hide_shape(circle);
	hide_shape(line);
	hide_shape(text);
	hide_shape(cross);
	
	dtk_hshape shapes[] = {circle, cross, line, text};

	comp = dtk_create_composite_shape(NULL, 
	                                  sizeof(shapes)/sizeof(shapes[0]),
					  shapes, 1);
}

static void cleanup_shapes(void) {
	dtk_destroy_shape(comp);
}


static void redraw(dtk_hwnd wnd) {
	dtk_clear_screen(wnd);
	dtk_draw_shape(comp);
	dtk_update_screen(wnd);
}


static int configure_trial(unsigned int trIdx) { 

	unsigned int event;

	switch(trIdx){
		case 0:	// LOOK LEFT
			show_shape(cross);
			hide_shape(line);
			show_shape(circle);
			dtk_move_shape(circle, LFT_POSX, LFT_POSY);
			event = EVT_TRL_LEFT;
			break;
		case 1: // LOOK RIGHT
			show_shape(cross);
			hide_shape(line);
			show_shape(circle);
			dtk_move_shape(circle, RGT_POSX, RGT_POSY);
			event = EVT_TRL_RIGHT;
			break;
		case 2: // LOOK UP
			show_shape(cross);
			hide_shape(line);
			show_shape(circle);
			dtk_move_shape(circle, TOP_POSX, TOP_POSY);
			event = EVT_TRL_TOP;
			break;
		case 3: // LOOK DOWN
			show_shape(cross);
			hide_shape(line);
			show_shape(circle);
			dtk_move_shape(circle, DWN_POSX, DWN_POSY);
			event = EVT_TRL_DOWN;
			break;
		case 4: // BLINK
			hide_shape(cross);
			hide_shape(circle);
			show_shape(line);
			event = EVT_TRL_BLINK;
			break;
		case 5: // FIXATE
			show_shape(cross);
			hide_shape(circle);
			hide_shape(line);
			event = EVT_TRL_FIX;
			break;
	}

	return event;
}

static int event_handler(dtk_hwnd wnd, int type, const union dtk_event* evt) {
	int retcode = 1;

	switch (type) {
		case DTK_EVT_QUIT:
			CcLogWarning("User asked to quit");
			retcode = 0;
			break;

		case DTK_EVT_REDRAW:
			redraw(wnd);
			break;
		case DTK_EVT_KEYBOARD:
			switch(evt->key.sym) {
				case DTKK_SPACE:
					waituser = false;
					hide_shape(text);
					redraw(wnd);
					break;
				case DTKK_ESCAPE:
					CcLogWarning("User asked to quit");
					retcode = 0;
					break;
				default:
					break;
			}
			break;
		default:
			break;
	}
	return retcode;
}

static void usage(void) { 
	printf("[vacolor_offline] - Usage: vacolor_offline [-f -h]\n\n");
	printf("  -f       Full screen [Default: 640x480]\n");
	printf("  -h       display this help and exit\n");
}



int main(int argc, char** argv)
{
	int opt;
	while((opt = getopt(argc, argv, "fh")) != -1) {
		switch(opt) {
			case 'f':
				WinHeight = 0;
				WinWidth  = 0;
				break;
			case 'h':
				usage();
				CcCore::Exit(0);
				break;
		}
	}
	
	
	//************* Loop Initialization ****************//
	CcCore::OpenLogger("eyeoffline");
	CcCore::CatchSIGINT();
	CcCore::CatchSIGTERM();
	ClLoop::Configure();
	
	// Connect to the loop
	CcLogInfo("Connecting to loop...");
	while(ClLoop::Connect() == false) {
		CcLogFatal("Cannot connect to loop");
		CcTime::Sleep(3000.0f);
	}
	CcLogInfo("Connected to the loop");
	
	// Configure Tools for TOBI iD for GDF Events
	id  = new ClTobiId(ClTobiId::SetOnly);
	idm = new IDMessage;
	ids = new IDSerializerRapid(idm);
	idm->SetDescription("eyeoffline");
	idm->SetFamilyType(IDMessage::FamilyBiosig);
	idm->SetEvent(0);
	
	// Attach iD
	while(id->Attach("/bus") == false) {
		CcLogFatal("Cannot attach iD");
		CcTime::Sleep(3000.0f);
	}
	CcLogInfo("Connected to iD /bus");
	//*************************************************//	

	
	//********* Initialize Drawtk objects *************//
	wnd = dtk_create_window(WinWidth, WinHeight, 0, 0, 16, "Eye artifacts - Protocol offline");
	dtk_set_event_handler(wnd, event_handler);
	
	setup_shapes();
	redraw(wnd);
	//*************************************************//	
	

	//********** Computing Trials sequence ***********//
	co_stimula_sequence trialseq;	
	unsigned int occur[TRL_NUMCLASSES] = {TRL_OCCURENCES, TRL_OCCURENCES, 
					      TRL_OCCURENCES, TRL_OCCURENCES,
					      TRL_OCCURENCES, TRL_OCCURENCES};
	unsigned int NumTrials = TRL_NUMCLASSES*TRL_OCCURENCES;
	trialseq = co_stimula_init(TRL_NUMCLASSES, occur, NULL);
	co_stimula_randomize(&trialseq);

	for (unsigned int i =0; i<NumTrials; i++)
		printf("[Trial %2d] - Class %d\n", i+1, trialseq.sequence[i]);
	//*************************************************//	
	
	//************* Waiting for user start ************//
	CcLogInfo("Waiting for user start");
	show_shape(text);
	redraw(wnd);
	while(waituser) {
		dtk_process_events(wnd);
		CcTime::Sleep(100.0f);
	}
	hide_shape(text);
	hide_shape(circle);
	dtk_move_shape(circle, 0.0f, 0.0f);
	redraw(wnd);
	CcLogInfo("User asked to start");
	//*************************************************//	

	//**************** Start protocol ****************//

	for (unsigned int i = 0; i < NumTrials; i++) {
	
		CcLogInfoS("Trial " <<i+1<<"/"<<NumTrials);
		// Trial start
		idm->SetEvent(EVT_TRL_START);
		id->SetMessage(ids);
		CcTime::Sleep(TRL_TIME_START);
		idm->SetEvent(EVT_TRL_START + EVT_TRL_OFF);
		id->SetMessage(ids);
		
		if (dtk_process_events(wnd) == 0)
			break;	
		
		// Trial configuration
		event = configure_trial(trialseq.sequence[i]);
		redraw(wnd);
		idm->SetEvent(event);
		id->SetMessage(ids);
		CcTime::Sleep(TRL_TIME_PERIOD);
		idm->SetEvent(event + EVT_TRL_OFF);
		id->SetMessage(ids);
		
		if (dtk_process_events(wnd) == 0)
			break;	
		
		// End Trial
		idm->SetEvent(EVT_TRL_END);
		id->SetMessage(ids);
	
		// Trial reset
		
		if (event == EVT_TRL_BLINK) {
			hide_shape(line);
			hide_shape(circle);
			show_shape(cross);
		} else if(event == EVT_TRL_FIX) {
			hide_shape(line);
			hide_shape(circle);
			show_shape(cross);
		} else {
			hide_shape(line);
			show_shape(circle);
			show_shape(cross);
			dtk_move_shape(circle, 0.0f, 0.0f);
		}
		redraw(wnd);
		
		CcTime::Sleep(500.0f);
		hide_shape(line);
		hide_shape(circle);
		show_shape(cross);
		redraw(wnd);
		
		idm->SetEvent(EVT_TRL_END + EVT_TRL_OFF);
		id->SetMessage(ids);

		CcTime::Sleep(TRL_TIME_END);
		
		if (dtk_process_events(wnd) == 0)
			break;	
	}
	
	//*************************************************//	

	// Cleanup all created resources 
	cleanup_shapes();
	
	dtk_close(wnd);

	delete id;
	delete idm;
	delete ids;
	CcCore::Exit(0);

}

